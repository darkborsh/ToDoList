# ToDoList

## Домашнее задание №13
- #### Доработать метод получения списка задач таким образом, чтобы обращение к внешнему REST API выполнялось параллельно к обращению к локальной БД, а затем данные собирались в единый список.