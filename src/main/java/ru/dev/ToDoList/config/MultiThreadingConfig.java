package ru.dev.ToDoList.config;

import lombok.Getter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
@Getter
public class MultiThreadingConfig {
    @Bean
    public TaskExecutor customThreadPoolTaskExecutor() {
        var executor = new ThreadPoolTaskExecutor();

        executor.setThreadNamePrefix("TaskExecutor-");
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(100);
        executor.initialize();

        return executor;
    }
}
