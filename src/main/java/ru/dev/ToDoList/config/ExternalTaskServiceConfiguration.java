package ru.dev.ToDoList.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ConfigurationProperties(prefix = "remote-service")
@Component
public class ExternalTaskServiceConfiguration {
    private final long connectionTimeout = 10;
    private final long readTimeout = 20;

    private String url;
    private String username;
    private String password;
}
