package ru.dev.ToDoList.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.model.DescriptionHolder;
import ru.dev.ToDoList.model.StatusHolder;
import ru.dev.ToDoList.service.GeneralTaskService;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/tasks")
public class TasksController {
    private final GeneralTaskService taskService;

    @Transactional
    @GetMapping
    public List<TaskDto> getTasks(@RequestParam (name = "substring", required = false) String substring,
                                  @RequestParam (name = "isAll", required = false) boolean isAll) throws ExecutionException, InterruptedException {
        return taskService.getAll(substring, isAll);
    }

    @PostMapping
    public ResponseEntity<TaskDto> saveTask(@Valid @RequestBody DescriptionHolder desc) {
        TaskDto t = taskService.save(null, desc);
        return ResponseEntity.created(URI.create("/tasks/" + t.getId())).body(t);
    }

    @Transactional
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTask(@PathVariable("id") @Min(1) long id,
                                             @RequestParam (name = "source", required = false) String source) {
        taskService.delete(source, id);
        return ResponseEntity.noContent().build();
    }

    @Transactional
    @PatchMapping("/{id}/completed")
    public ResponseEntity<String> statusUpdate(@PathVariable("id") @Min(1) long id,
                                               @Valid @RequestBody StatusHolder status,
                                               @RequestParam (name = "source", required = false) String source) {
        if (taskService.updateStatus(source, id, status.isCompleted())) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @Transactional
    @PatchMapping("/{id}")
    public ResponseEntity<String> descriptionUpdate(@PathVariable("id") @Min(1) long id,
                                                    @Valid @RequestBody DescriptionHolder desc,
                                                    @RequestParam (name = "source", required = false) String source) {
        if (taskService.updateDescription(source, id, desc.getDescription())) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
