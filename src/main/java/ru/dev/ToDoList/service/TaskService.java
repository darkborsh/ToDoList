package ru.dev.ToDoList.service;

import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.model.DescriptionHolder;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface TaskService {
    List<TaskDto> getAll(String substring, boolean includeCompleted) throws ExecutionException, InterruptedException;
    TaskDto save(String source, DescriptionHolder description);
    void delete(String source, long taskId);
    boolean updateDescription(String source, long taskId, String newDescription);
    boolean updateStatus(String source, long taskId, boolean status);
}
