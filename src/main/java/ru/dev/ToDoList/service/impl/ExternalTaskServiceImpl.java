package ru.dev.ToDoList.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.dev.ToDoList.config.ExternalTaskServiceConfiguration;
import ru.dev.ToDoList.dto.ExternalTaskDto;
import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.dto.mappers.ExternalTaskMapper;
import ru.dev.ToDoList.model.DescriptionHolder;
import ru.dev.ToDoList.service.CustomTaskService;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static ru.dev.ToDoList.dto.mappers.ExternalTaskMapper.SOURCE;

@Slf4j
@Service
public class ExternalTaskServiceImpl implements CustomTaskService {
    private final RestTemplate restTemplate;
    private final ExternalTaskMapper taskMapper;

    public ExternalTaskServiceImpl(RestTemplateBuilder builder, ExternalTaskServiceConfiguration config, ExternalTaskMapper taskMapper) {
        this.taskMapper = taskMapper;
        restTemplate = builder
                .rootUri(config.getUrl())
                .setConnectTimeout(Duration.ofSeconds(config.getConnectionTimeout()))
                .setReadTimeout(Duration.ofSeconds(config.getReadTimeout()))
                .basicAuthentication(config.getUsername(),config.getPassword())
                .build();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    private Optional<ExternalTaskDto> getById(long taskId) {
        Map <String, String> params = new HashMap<>();
        params.put("id", String.valueOf(taskId));
        try {
            return Optional.ofNullable(restTemplate.getForEntity("/tasks/{id}", ExternalTaskDto.class, params).getBody());
        } catch (HttpClientErrorException ex) {
            log.error("External db haven't task with this id" + params);
        }
        return Optional.empty();
    }

    private boolean updateAction(long taskId, Consumer<ExternalTaskDto> consumer) {
        Optional<ExternalTaskDto> task = getById(taskId);
        if (task.isPresent()) {
            consumer.accept(task.get());
            String url = "/tasks/" + taskId;
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PATCH, new HttpEntity<>(task.get()), String.class);
            return response.getStatusCode() == HttpStatus.OK;
        }
        return false;
    }

    @Async
    @Override
    public Future<List<TaskDto>> getAllTasks(String substring, boolean includeCompleted) {
        List<TaskDto> result = taskMapper.toDtoList(
                Arrays.asList(Objects.requireNonNull(restTemplate.getForEntity("/tasks", ExternalTaskDto[].class)
                        .getBody()))
        );

        result.forEach(t -> t.setSource(SOURCE));

        if (substring != null) {
            result =  result.stream().filter(t -> t.getDescription().contains(substring)).collect(Collectors.toList());
        }

        if (!includeCompleted) {
            result = result.stream().filter(t -> !t.isCompleted()).collect(Collectors.toList());
        }

        return AsyncResult.forValue(result);
    }

    @Override
    public List<TaskDto> getAll(String substring, boolean includeCompleted) {
        return null;
    }

    @Override
    public TaskDto save(String source, DescriptionHolder description) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(String source, long taskId) {
        Map <String, String> params = new HashMap<>();
        params.put("id", String.valueOf(taskId));
        restTemplate.delete("/tasks/{id}", params);
    }

    @Override
    public boolean updateDescription(String source, long taskId, String newDescription) {
        return updateAction(taskId, t -> t.setDescription(newDescription));
    }

    @Override
    public boolean updateStatus(String source, long taskId, boolean status) {
        return updateAction(taskId, t -> t.setDone(status));
    }

    @Override
    public boolean supports(String source) {
        return source != null && source.equals(SOURCE);
    }
}
