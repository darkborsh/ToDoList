package ru.dev.ToDoList.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.model.DescriptionHolder;
import ru.dev.ToDoList.service.CustomTaskService;
import ru.dev.ToDoList.service.GeneralTaskService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@RequiredArgsConstructor
@Component
public class GeneralTaskServiceImpl implements GeneralTaskService {
    private final List<CustomTaskService> taskServices;

    private CustomTaskService selectService(String source) {
        Optional<CustomTaskService> neededService = taskServices
                .stream()
                .filter(s -> s.supports(source))
                .findFirst();

        if (neededService.isPresent()) {
            return neededService.get();
        }

        throw new UnsupportedOperationException();
    }

    @Override
    public List<TaskDto> getAll(String substring, boolean includeCompleted) throws ExecutionException, InterruptedException {
        List<TaskDto> generalTaskList = new ArrayList<>();
        List<Future<List<TaskDto>>> listOfFutureLists = new ArrayList<>();

        for (CustomTaskService ts : taskServices) {
            listOfFutureLists.add(ts.getAllTasks(substring, includeCompleted));
        }

        for (Future<List<TaskDto>> futureList : listOfFutureLists) {
            try {
                generalTaskList.addAll(futureList.get());
            } catch (InterruptedException | ExecutionException | NullPointerException e) {
                e.printStackTrace();
                log.error("Something going wrong with one of lists");
            }
        }
        return AsyncResult.forValue(generalTaskList).get();
    }

    @Override
    public TaskDto save(String source, DescriptionHolder description) {
        return selectService(source).save(source, description);
    }

    @Override
    public void delete(String source, long taskId) {
        selectService(source).delete(source, taskId);
    }

    @Override
    public boolean updateDescription(String source, long taskId, String newDescription) {
        return selectService(source).updateDescription(source, taskId, newDescription);
    }

    @Override
    public boolean updateStatus(String source, long taskId, boolean status) {
        return selectService(source).updateStatus(source, taskId, status);
    }
}
