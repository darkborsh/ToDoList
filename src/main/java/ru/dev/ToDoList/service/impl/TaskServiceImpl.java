package ru.dev.ToDoList.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.dev.ToDoList.dao.TaskDao;
import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.dto.UserDto;
import ru.dev.ToDoList.dto.mappers.TaskMapper;
import ru.dev.ToDoList.dto.mappers.UserMapper;
import ru.dev.ToDoList.model.DescriptionHolder;
import ru.dev.ToDoList.service.CustomTaskService;
import ru.dev.ToDoList.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static ru.dev.ToDoList.dto.mappers.TaskMapper.SOURCE;

@RequiredArgsConstructor
@Service
public class TaskServiceImpl implements CustomTaskService {
    private final TaskDao taskDao;
    private final TaskMapper taskMapper;
    private final UserService userService;
    private final UserMapper userMapper;

    private Optional<String> getUsernameByContext() {
        return Optional.of(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    private Optional<UserDto> getUserByContext() {
        if (getUsernameByContext().isPresent()) {
            return userService.getUserByName(getUsernameByContext().get());
        }
        return Optional.empty();
    }

    private Optional<TaskDto> get(long taskId) {
        Optional<UserDto> user = getUserByContext();
        return user.map(userDto -> taskMapper.toDto(taskDao.findByIdAndUserId(taskId, userDto.getId())));
    }

    @Async
    public Future<List<TaskDto>> getAndFilterTasks(Optional<UserDto> user, String substring, boolean includeCompleted) {
        List<TaskDto> result = new ArrayList<>();
        if (user.isPresent()) {
            result = taskMapper.toDtoList(taskDao.find(user.get().getId(), substring, includeCompleted));
        }
        return AsyncResult.forValue(result);
    }

    @Override
    public Future<List<TaskDto>> getAllTasks(String substring, boolean includeCompleted) {
        Optional<UserDto> user = getUserByContext();
        return getAndFilterTasks(user, substring, includeCompleted);
    }

    @Override
    public List<TaskDto> getAll(String substring, boolean includeCompleted) throws ExecutionException, InterruptedException {
        return null;
    }

    @Override
    public TaskDto save(String source, DescriptionHolder description) {
        return taskMapper.toDto(taskDao.save(taskDao.build(description.getDescription(), userMapper.dtoToUser(getUserByContext().get()))));
    }

    @Override
    public void delete(String source, long taskId) {
        if (this.get(taskId).isPresent()) {
            taskDao.deleteById(taskId);
        }
    }

    @Override
    public boolean updateDescription(String source, long taskId, String newDescription) {
        return taskDao.update(taskId, getUserByContext().get().getId(), t -> t.setDescription(newDescription));
    }

    @Override
    public boolean updateStatus(String source, long taskId, boolean status) {
        return taskDao.update(taskId, getUserByContext().get().getId(), t -> t.setCompleted(status));
    }

    @Override
    public boolean supports(String source) {
        return source == null || source.equals("") || source.equals(SOURCE);
    }
}
