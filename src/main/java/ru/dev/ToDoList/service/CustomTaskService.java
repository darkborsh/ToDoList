package ru.dev.ToDoList.service;

import ru.dev.ToDoList.dto.TaskDto;

import java.util.List;
import java.util.concurrent.Future;

public interface CustomTaskService extends TaskService {
    Future<List<TaskDto>> getAllTasks(String substring, boolean includeCompleted);
    boolean supports(String source);
}
