package ru.dev.ToDoList.dto.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.dev.ToDoList.dto.ExternalTaskDto;
import ru.dev.ToDoList.dto.TaskDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExternalTaskMapper {
    static final String SOURCE = "kalchenko";

    @Mapping(target = "source", constant = "SOURCE")
    @Mapping(source = "done", target = "completed")
    @Mapping(target = "user", constant = "general")
    List<TaskDto> toDtoList(List<ExternalTaskDto> externalTaskDtoList);

    @Mapping(target = "source", constant = "SOURCE")
    @Mapping(source = "done", target = "completed")
    @Mapping(target = "user", constant = "general")
    TaskDto toDto(ExternalTaskDto externalTaskDto);
}
