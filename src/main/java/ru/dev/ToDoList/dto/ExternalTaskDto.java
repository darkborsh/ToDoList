package ru.dev.ToDoList.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ExternalTaskDto {
    private long id;
    private String description;
    private boolean done;
}
