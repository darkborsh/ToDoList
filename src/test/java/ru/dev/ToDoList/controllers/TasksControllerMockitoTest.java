package ru.dev.ToDoList.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.model.DescriptionHolder;
import ru.dev.ToDoList.model.StatusHolder;
import ru.dev.ToDoList.service.GeneralTaskService;

import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class TasksControllerMockitoTest {
    private GeneralTaskService taskService;
    private TasksController tasksController;

    @BeforeEach
    public void setup() {
        taskService = mock(GeneralTaskService.class);
        tasksController = new TasksController(taskService);
    }

    @Test
    public void getTasks_DoNotReturnNull_Always() throws ExecutionException, InterruptedException {
        final List<TaskDto> tasksListWithNullSubstring = tasksController.getTasks(null, false);
        final List<TaskDto> tasksListWithNotNullSubstring = tasksController.getTasks("anyString", true);

        assertNotNull(tasksListWithNullSubstring);
        assertNotNull(tasksListWithNotNullSubstring);
    }

    @Test
    public void getTasks_ReturnsCorrectTaskList_Always() throws ExecutionException, InterruptedException {
        TaskDto task = new TaskDto();
        final String username = "Ivan";
        task.setUser(username);
        task.setId(1);
        task.setDescription("do something");
        final List<TaskDto> expectedList = List.of(task);

        when(taskService.getAll(anyString(), anyBoolean())).thenReturn(expectedList);

        List<TaskDto> taskListFromMethod = tasksController.getTasks("do something", false);

        assertEquals(expectedList, taskListFromMethod);
        verify(taskService).getAll("do something", false);
        verifyNoMoreInteractions(taskService);
    }

    @Test
    public void saveTask_SaveTaskAndReturnsLocation_Always() {
        final DescriptionHolder newTaskDescription = new DescriptionHolder();
        newTaskDescription.setDescription("anyString");
        TaskDto task = new TaskDto();
        final String username = "Ivan";
        task.setUser(username);
        task.setId(1);
        task.setDescription(newTaskDescription.getDescription());
        when(taskService.save(any(), eq(newTaskDescription))).thenReturn(task);

        ResponseEntity<TaskDto> response = tasksController.saveTask(newTaskDescription);
        URI actualLocation = response.getHeaders().getLocation();

        assertEquals(task, response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(actualLocation);
        assertEquals("/tasks/" + task.getId(), actualLocation.toString());

        verify(taskService).save(any(), eq(newTaskDescription));
        verifyNoMoreInteractions(taskService);
    }

    @Test
    public void deleteTask_DeleteTask_Always() {
        tasksController.deleteTask(anyLong(), anyString());

        verify(taskService).delete(anyString(), anyLong());
        verifyNoMoreInteractions(taskService);
    }

    @Test
    public void statusUpdate_ThrowsNotFound_IfTaskNotFound() {
        when(taskService.updateStatus(anyString(), anyLong(), anyBoolean())).thenReturn(false);

        ResponseEntity<String> response = tasksController.statusUpdate(1, new StatusHolder(), null);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void statusUpdate_ChangeTaskStatus_IfTaskFound() {
        when(taskService.updateStatus(anyString(), anyLong(), anyBoolean())).thenReturn(true);

        ResponseEntity<String> response = tasksController.statusUpdate(1, new StatusHolder(), "");

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void descriptionUpdate_ChangeTaskDescription_IfTaskFound() {
        when(taskService.updateDescription(anyString(), anyLong(), anyString())).thenReturn(true);
        DescriptionHolder desc = new DescriptionHolder();
        desc.setDescription("something");

        ResponseEntity<String> response = tasksController.descriptionUpdate(1, desc, "null");

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void descriptionUpdate_ThrowsNotFound_IfTaskNotFound() {
        when(taskService.updateDescription(anyString(), anyLong(), anyString())).thenReturn(false);

        ResponseEntity<String> response = tasksController.descriptionUpdate(1, new DescriptionHolder(), null);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
