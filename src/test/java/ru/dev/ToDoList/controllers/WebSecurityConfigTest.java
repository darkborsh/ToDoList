package ru.dev.ToDoList.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.dev.ToDoList.config.WebSecurityConfig;
import ru.dev.ToDoList.service.impl.UserDetailsServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class WebSecurityConfigTest {
    @Autowired
    private WebSecurityConfig config;

    @Test
    public void loadContext() throws Exception {
        assertThat(config).isNotNull();
        assertThat(config.getUserDetailsService()).isNotNull();
    }

    @Test
    public void WebSecurityConfig_CorrectBeanChose_Always() throws Exception {
        assertThat(config.getUserDetailsService()).isInstanceOf(UserDetailsServiceImpl.class);
        assertThat(config.passwordEncoder()).isInstanceOf(BCryptPasswordEncoder.class);
    }
}
