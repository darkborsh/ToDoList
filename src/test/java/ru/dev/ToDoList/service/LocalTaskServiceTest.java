package ru.dev.ToDoList.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import ru.dev.ToDoList.dto.TaskDto;
import ru.dev.ToDoList.dto.mappers.TaskMapper;
import ru.dev.ToDoList.dto.mappers.UserMapper;
import ru.dev.ToDoList.model.DescriptionHolder;
import ru.dev.ToDoList.model.Task;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ComponentScan(basePackages = "ru.dev")
@DataJpaTest
@ContextConfiguration(classes = {RestTemplateAutoConfiguration.class})
@Sql(statements = "insert into users (name, password, role) VALUES ('user', '$2a$10$8mVoN4asMxaxisoXPG68SeGGuy932wBRGSkMT2EfgAhyatNcrzWn.', 'USER')")
public class LocalTaskServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    @Qualifier("taskServiceImpl")
    @Autowired
    private CustomTaskService taskService;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private UserMapper userMapper;

    @Test
    @WithUserDetails
    public void updateDescription_ChangesDescriptionAndSaveTasksProperly_WhenTaskFoundAndAlways() throws ExecutionException, InterruptedException {
        String newDesc = "new description";

        DescriptionHolder descHolder = new DescriptionHolder();
        descHolder.setDescription("description for task from db");

        Task task = new Task();
        task.setDescription("description for local test task");
        task.setUser(userMapper.dtoToUser(userService.getUserByName("user").get()));

        entityManager.persist(task);
        task.setDescription(newDesc);
        entityManager.merge(task);
        long idOfTestTask = task.getId();
        TaskDto testTask = taskMapper.toDto(entityManager.find(Task.class, idOfTestTask));

        TaskDto testTaskFromDb = taskService.save(null, descHolder);
        long idOfTestTaskFromDb = testTaskFromDb.getId();
        taskService.updateDescription(null, idOfTestTaskFromDb, newDesc);
        List<TaskDto> taskListFromDb = taskService.getAllTasks(newDesc, false).get();

        assertEquals(newDesc, testTask.getDescription());
        assertEquals(newDesc, taskListFromDb.get((int) idOfTestTaskFromDb - 1).getDescription());
        assertEquals(2, taskListFromDb.size());
    }
}
