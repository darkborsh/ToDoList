package ru.dev.ToDoList.service;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import ru.dev.ToDoList.dto.TaskDto;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class ExternalTaskServiceTest {

    static final String USER = "user";
    static final String PASSWORD = "123";

    @RegisterExtension
    static WireMockExtension wm = WireMockExtension.newInstance()
            .options(wireMockConfig().dynamicPort())
            .build();

    @DynamicPropertySource
    static void registerDynamicProperties (DynamicPropertyRegistry registry) {
        registry.add("remote-service.url", wm::baseUrl);
        registry.add("remote-service.username", () -> USER);
        registry.add("remote-service.password", () -> PASSWORD);
    }

    @Qualifier("externalTaskServiceImpl")
    @Autowired
    CustomTaskService taskService;

    @Test
    void getAll_ReturnsCorrectListByParameters_Always(WireMockRuntimeInfo wmRuntimeInfo) throws ExecutionException, InterruptedException {
        String substring = "in";
        String desc  = "Something " + substring + " description";
        WireMock wireMock = wmRuntimeInfo.getWireMock();

        wireMock.register(get("/tasks").willReturn(okJson("[" +
                "{" +
                "\"id\": 5," +
                "\"done\": \"false\"," +
                "\"description\": \"Something in description\"" +
                "}"+
                "]")));

        List<TaskDto> taskList = taskService.getAllTasks(substring, false).get();

        assertEquals(1, taskList.size());
        var task = taskList.get(0);
        assertFalse(task.isCompleted());
        assertEquals(desc, task.getDescription());
        assertEquals(5, task.getId());
        assertEquals("kalchenko", task.getSource());
    }
}