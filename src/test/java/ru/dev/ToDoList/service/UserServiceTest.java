package ru.dev.ToDoList.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import ru.dev.ToDoList.containers.PostgresContainerInstance;
import ru.dev.ToDoList.dto.UserDto;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ComponentScan(basePackages = "ru.dev")
@DataJpaTest
@ContextConfiguration(classes = {RestTemplateAutoConfiguration.class})
@Sql(statements = "insert into users (name, password, role) VALUES ('user', '$2a$10$8mVoN4asMxaxisoXPG68SeGGuy932wBRGSkMT2EfgAhyatNcrzWn.', 'ADMIN')")
public class UserServiceTest extends PostgresContainerInstance {
    @Autowired
    private UserService userService;

    @Test
    public void setUp() {
        postgres.start();

        Assertions.assertTrue(postgres.isRunning());
    }

    @Test
    @WithUserDetails
    public void getUsers_ReturnsCorrectUser_WhenUserFound() {
        List<UserDto> users = userService.getUsers();
        assertEquals("user", users.get(0).getName());
    }
}
