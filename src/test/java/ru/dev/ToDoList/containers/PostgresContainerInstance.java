package ru.dev.ToDoList.containers;

import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public abstract class PostgresContainerInstance {
    @Container
    public static TestPostgresContainer postgres = TestPostgresContainer.getInstance();
}
